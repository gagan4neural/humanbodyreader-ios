//
//  ApplicationFeatureGridCell.swift
//  AVPlayer
//
//  Created by Gagan on 18/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class ApplicationFeatureGridCell: BaseCollectionViewCell, BaseCellProtocol {
    
    @IBOutlet weak var bgView: NeuralBaseView!
    @IBOutlet weak var image: NeuralBaseImageView!
    @IBOutlet weak var icon: NeuralIconLabel!
    @IBOutlet weak var title: NeuralBaseLabel!
    
    static let reuseIdentifier = "ApplicationFeatureGridCell";
    static let nibName = "ApplicationFeatureGridCell";
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func baseInit() {
        self.backgroundColor = .clear;
        self.bgView.backgroundColor = .clear;
        self.icon.backgroundColor = .clear;
        self.image.backgroundColor = .clear;
        self.title.backgroundColor = .clear;
        self.title.textAlignment = .left;

        self.title.font = FontManager.shared.helveticaNeue(16);

//        self.bgView.clipsToBounds = true;
        self.bgView.layer.borderColor = ColorUtils.instance().highlightedColor.cgColor;
        
        self.bgView.layer.shadowColor = UIColor.black.cgColor;
        self.bgView.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.bgView.layer.shadowRadius = 4;
        self.bgView.layer.shadowOpacity = 0.8;
    }
    
    func setTitle(_ title:String) -> Void{
        self.title.text = title;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        self.bgView.layer.cornerRadius = min(self.bgView.frame.width, self.bgView.frame.height) * 0.5;
    }
}
