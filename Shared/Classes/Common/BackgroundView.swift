//
//  BackgroundView.swift
//  AVPlayer
//
//  Created by Gagan on 15/09/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class BackgroundView: UIView {

    static func instance () -> BackgroundView? {
        
        let view : BackgroundView? = BundleManager.loadNib(name: "BackgroundView", owner: self, options: nil);
        view?.loadView();
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight];
        return view
    }
    
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var iconLabel : NeuralIconLabel!
    @IBOutlet weak var viewContainer: NeuralBaseView! // all other subviews must reside here.
    @IBOutlet weak var tableView: NeuralBaseTableView!
    @IBOutlet var collectionView: NeuralBaseCollectionView!
    
    
    private func loadView() {
        let image = NeuralLabManager.shared.getBackgroundImage()
        self.imageView.image = image;
        self.iconLabel.isHidden = true;
    }
}
