//
//  BaseViewController.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 19/05/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class BaseViewController: NeuralBaseViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var bgView : BackgroundView? = nil;
    var headerView : HeaderView? = nil;
    var collectionView : NeuralBaseCollectionView? = nil;
    var containerView : NeuralBaseView {
        get{
            return self.bgView?.viewContainer ?? NeuralBaseView();
        }
    };
    var collectionViewReusage:[AnyHashable: Any] = [AnyHashable: Any]();
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        bgView = BackgroundView.instance();
        if (bgView != nil){
            self.collectionView = self.bgView?.collectionView;
            self.view.insertSubview(bgView!, at: 0);
        }
        
        self.loadHeaderView();
    }

    
    
    //MARK: View Methods
    func loadHeaderView () {
        
        weak var weakSelf = self;
        if (headerView == nil){
            headerView = HeaderView.instance();
            headerView?.leftBtnCallback = { (val) in
                weakSelf!.leftButtonTapped()
            }
            headerView?.rightBtnCallback = { (val) in
                weakSelf!.rightButtonTapped()
            }
            self.view.addSubview(headerView!);
        }
        
        headerView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: nlUtils.shared.titlebarHeight())
    }
    
    func addSubView (view: UIView!){
        self.bgView?.viewContainer?.addSubview(view);
    }
    
    func hideBackButton () {
        self.headerView?.btnLeft.isHidden = true;
    }
    
    func leftButtonTapped(){
        if (self.isPresented()){
            nlUtils.shared.dismissAllPopOvers(nil);
        }else{
            self.navigationController?.popViewController(animated: true);
        }
    }
    
    func rightButtonTapped(){
    }
    
    func refreshView () {
        if (self.collectionView?.isHidden == false){
            self.collectionView?.reloadData();
        }
    }
    
    
    
    //MARK: CollectionView Methods
    func setUpCollectionView () {
        self.collectionView?.delegate = self;
        self.collectionView?.dataSource = self;
        self.collectionView?.backgroundColor = .clear;
        self.collectionView?.isHidden = false;
        self.collectionView?.isPrefetchingEnabled = true;
        self.collectionView?.isScrollEnabled = true;
    }
    
    func loadCollectionView(withCellClass cellClass:AnyClass?, reuseIdentifier:String){
        self.setUpCollectionView();
        if (cellClass != nil){
            self.collectionView?.register(cellClass, forCellWithReuseIdentifier: reuseIdentifier);
        }
        else{
            self.collectionView?.register(BaseCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: reuseIdentifier);
        }
        self.refreshView();
    }
    
    func loadCollectionView(withNibName nibName:String, reuseIdentifier:String){
        self.setUpCollectionView();
        self.collectionView?.register(UINib(nibName: nibName, bundle: Bundle.main), forCellWithReuseIdentifier:reuseIdentifier);
        self.refreshView();
    }
    
    func bottomInsetMargin () -> CGFloat{
        let bMargin: CGFloat = self.layoutAllowsBannerAds() ?  NeuralLabManager.bannerHeight(forView: self.view) : 0;
        return bMargin
    }
    
    func topInsetMargin () -> CGFloat{
        let tMargin = (self.headerView?.isHidden ?? false) ? 0 : (self.headerView?.frame.height ?? 0);
        return tMargin
    }
    
    func leftInsetMargin () -> CGFloat{
        return 0
    }
    
    func rightInsetMargin () -> CGFloat{
        return 0
    }
    
    func adjustViewInsets () {
        let t = topInsetMargin();
        let b = bottomInsetMargin();
        let l = leftInsetMargin();
        let r = rightInsetMargin();
        
        if (self.collectionView?.isHidden == false){
            self.collectionView?.contentInset = UIEdgeInsets(top: t, left: l, bottom: b, right: r)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = BaseCollectionViewCell.dequeueBaseCell(collectionView, indexPath: indexPath, reuseIdentifier: nil);
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return nlUtils.shared.collectionViewSize();
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return nlUtils.shared.isIpad() ? 30 : 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return nlUtils.shared.isIpad() ? 30 : 10;
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //isTransitionAvailable = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //view.endEditing(true)
    }
    
    
    //MARK: Layouting
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        self.bgView?.frame = self.view.bounds;
        self.layoutHeaderView();
    }
    
    func layoutHeaderView () {
        if (self.headerView != nil){
            var rectHeaderView = self.headerView?.frame;
            rectHeaderView?.size.height = nlUtils.shared.titlebarHeight();
            self.headerView?.frame = rectHeaderView!
        }
    }
    
}
