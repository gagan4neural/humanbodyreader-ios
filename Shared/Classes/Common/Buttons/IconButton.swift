//
//  IconButton.swift
//  AVPlayer
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class IconButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.initializeButton();
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeButton();
    }
    
    private func initializeButton () {
        self.titleLabel?.font = FontManager.shared.iconFont(size: nlUtils.shared.isIpad() ? 30 : 25);
        
        self.setTitleColor(ColorUtils.instance().defaultColor, for: .normal);
        self.setTitleColor(.black, for: .highlighted);
        
        self.backgroundColor = .clear;
    }
    
}
