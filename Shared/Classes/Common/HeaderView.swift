//
//  HeaderView.swift
//  AVPlayer
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class HeaderView: UIView {

    static func instance () -> HeaderView? {
        
        let view : HeaderView? = BundleManager.loadNib(name: "HeaderView", owner: self, options: nil);
        view?.autoresizingMask = [.flexibleWidth];
        view?.sharedInit();
        return view
    }

    @IBOutlet weak var btnLeft: IconButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRight: IconButton!
    
    var leftBtnCallback: completion? = nil;
    var rightBtnCallback: completion? = nil;

    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    
    func sharedInit () {
        self.layer.shadowColor = ColorUtils.instance().shadowColor(.Title).cgColor;
        self.layer.shadowRadius = ColorUtils.instance().shadowRadius(.Title);
        self.layer.shadowOffset = CGSize(width: 0, height: 1);
        self.layer.shadowOpacity = 0.8;
        self.backgroundColor = .clear;
        
        self.lblTitle.text = NeuralLabManager.appName;
        self.lblTitle.font = FontManager.shared.helveticaNeue(nlUtils.shared.titleFontSize())
        self.btnLeft.setTitle(IconFontCodes.chevronLeft, for: .normal);
        
        self.lblTitle.backgroundColor = .clear;
        self.lblTitle.textColor = ColorUtils.instance().textColor;
        
        self.btnRight.isHidden = true
    }
    

//    var rightBtnTitle : String = "" {
//        didSet {
//            self.btnRight.titleLabel?.text = rightBtnTitle;
//        }
//    }
//
//    var viewBlurRadius: CGFloat = 0 {
//        didSet {
//            self.blurRadius = viewBlurRadius;
//        }
//    }
//
//    var viewBlurRatio: CGFloat = 0 {
//        didSet {
//            self.blurRatio = viewBlurRatio;
//        }
//    }
//
//    var viewBlendColor: UIColor? = nil {
//        didSet {
//            self.blendColor = viewBlendColor;
//        }
//    }
//
//    var viewDeepRendering: Bool = false {
//        didSet {
//            self.isDeepRendering = viewDeepRendering;
//        }
//    }
    
    @IBAction func onBtnTap(_ sender: IconButton) {
        print("Button tap");
        if (sender.isEqual(self.btnLeft) && (self.leftBtnCallback != nil)){
            self.leftBtnCallback!(true);
        }
        else if (sender.isEqual(self.btnRight) && (self.rightBtnCallback != nil)){
            self.rightBtnCallback!(true);
        }
    }
}
