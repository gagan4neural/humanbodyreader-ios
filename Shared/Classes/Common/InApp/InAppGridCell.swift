//
//  InAppGridCell.swift
//  AVPlayer
//
//  Created by Gagan on 19/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import NeuralLabs_iOS


class InAppGridCell: BaseCollectionViewCell, BaseCellProtocol {
    
    static let reuseIdentifier = "InAppGridCell";
    static let nibName = "InAppGridCell";

    @IBOutlet weak var bgView: NeuralBaseView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var lblPurchased: NeuralIconLabel!
    @IBOutlet weak var lblPrice: NeuralBaseLabel!
    @IBOutlet weak var lblTitle: NeuralBaseLabel!

    
    func setTitle(_ title: String) {
        self.lblTitle.text = title;
    }

    func baseInit() {
        self.bgView.backgroundColor = .clear;
        self.lblPurchased.backgroundColor = .clear;
        self.lblTitle.backgroundColor = .clear;
        self.lblPrice.backgroundColor = .clear;
        
        self.lblTitle.font = FontManager.shared.helveticaNeue(13);
        self.lblPurchased.text = IconFontCodes.check;
        self.lblPurchased.textColor = ColorUtils.instance().doneButtonColor;
        self.lblPrice.font = FontManager.shared.helveticaNeue(20);
        self.lblPurchased.font = FontManager.shared.iconFont(size: 13);

        self.bgView.layer.borderColor = ColorUtils.instance().highlightedColor.cgColor;
        
        self.bgView.clipsToBounds = true;
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews();
        self.bgView.layer.cornerRadius = min(self.bgView.frame.width, self.bgView.frame.height) * 0.5;
    }
}
