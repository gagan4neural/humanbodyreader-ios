//
//  InAppViewController.swift
//  AVPlayer
//
//  Created by Gagan on 19/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit

import NeuralLabs_iOS


fileprivate let InAppViewListHeight:CGFloat = 80
fileprivate let InAppViewCollectionMinimumHeight:CGFloat = 100;

class InAppViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadHeaderView();
        self.loadCollectionView(withNibName: InAppGridCell.nibName, reuseIdentifier: InAppGridCell.reuseIdentifier);
        
        self.headerView?.lblTitle.text = "In Apps"
        self.headerView?.btnRight.setTitle(IconFontCodes.time_restore_setting, for:.normal);
        self.headerView?.btnRight.isHidden = false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.adjustViewInsets();
        if self.isPresented(){
            self.headerView?.btnLeft.setTitle(IconFontCodes.close, for:.normal);
        }
        self.hideNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        if (NeuralLabManager.isInAppsReady == false){
            self.showHud("Retereiving Inapps");
        }
    }
    
    override func leftButtonTapped() {
        if nlUtils.shared.isIpad(){
            self.safeDismiss(animated: true)
        }else{
            self.safePop(animated: true)
        }
    }
    
    override func deviceOrientationChange(_ orientation: UIDeviceOrientation) {
        self.adjustViewInsets();
    }
    
    override func rightButtonTapped() {
        self.restoreInAppsProducts();
    }

    //MARK: Collection View
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NeuralLabManager.allInApps.count;
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell: InAppGridCell = InAppGridCell.dequeue(collectionView, indexPath: indexPath, reuseIdentifier: InAppGridCell.reuseIdentifier, nibName: InAppGridCell.nibName, cellBundle: Bundle.main)
        cell.baseInit();
        
        let inApp = NeuralLabManager.allInApps[indexPath.row];
        cell.setTitle(inApp.title ?? "");
        cell.lblPrice.text = inApp.localProductPrice;
        cell.lblPurchased.isHidden = !(inApp.purchased);
        
        collectionViewReusage[indexPath] = cell;
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let rect: CGSize = CGSize(width: InAppViewCollectionMinimumHeight, height: InAppViewCollectionMinimumHeight)
        return rect;
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? InAppGridCell{
            cell.bgView.layer.borderWidth = 2;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? InAppGridCell{
            cell.bgView.layer.borderWidth = 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let inApp = NeuralLabManager.allInApps[indexPath.row];
        weak var weakSelf = self;
        if (inApp.identifier != nil){
            self.showHud("Purchasing product")
            NeuralLabManager.purchaseProduct(inApp.identifier!) { (success, error) in
                
                nlUtils.shared.hideHud(from: weakSelf!.view);
                
                DispatchQueue.main.async(execute: {
                    if (success != nil){
                        weakSelf!.showToast("Successfully purchased product");
                        weakSelf?.collectionView?.reloadData();
                        if (inApp.identifier == NeuralLabManager.removeAdId){
                            self.showBannerAds();
                            NotificationController.noti_RemoveAdsPurchased()
                        }
                    }else{
                        weakSelf!.showToast("Failed to purchase product : \(String(describing: error))");
                    }
                });
            }
        }else{
            self.showToast("Failed to purchase product.");
        }
    }
}
