//
//  ApplicationDataModel.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 19/05/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import Foundation
import UIKit
import NeuralLabs_iOS


enum ApplicationFeatureType: String{
    case UnKnown = "Unknown";
    case PalmReader = "Palm Reader";
    case InApps = "In Apps";
    case Feedback = "Feedback";
    case MoreApps = "More Apps";
    case About = "About";
    case Facebook = "Facebok";
    case RestoreInApps = "Restore";
}


typealias FeatureTupple = (String, String?, String?, UIColor, UIColor, CGFloat)



struct Feature {
    
    var title: String = "";
    var icon: String? = nil;
    var bgColor: UIColor = .white;
    var image: String? = nil; // by default image is on priority then icon
    var shadowColor: UIColor = .black;
    var shadowRadius: CGFloat = 1;
    var titleColor: UIColor = ColorUtils.instance().featureColor(type: .TextColor);
    
    var feature:ApplicationFeatureType = .UnKnown;
    
    init(_ tupple:FeatureTupple, f:ApplicationFeatureType) {
        self.title = tupple.0;
        self.icon = tupple.1;
        self.image = tupple.2;
        self.bgColor = tupple.3;
        self.shadowColor = tupple.4;
        self.shadowRadius = tupple.5;
        
        feature = f;
    }
}

class ApplicationFeatureModel{
    
    var features: [Feature] = [Feature]();
    let defFeatureTupple: FeatureTupple = ("Title", nil, nil, .white, .black, 1);
    
    var audioFeatures: [Feature] = [Feature]();
    var videoFeatures: [Feature] = [Feature]();
    var imageFeatures: [Feature] = [Feature]();
    
    init() {
        features.append(Feature(self.featureValues(.PalmReader), f: .PalmReader));
        if (NeuralLabManager.canShowActiveInAppsToUser()){
            // now show the inapps to user based on firebase. Till the time dialog arrives In apps were supposed to be retereived from the iTunes.
            features.append(Feature(self.featureValues(.InApps), f: .InApps));
            features.append(Feature(self.featureValues(.RestoreInApps), f: .RestoreInApps));
        }
        features.append(Feature(self.featureValues(.Feedback), f: .Feedback));
        features.append(Feature(self.featureValues(.MoreApps), f: .MoreApps));
        features.append(Feature(self.featureValues(.Facebook), f: .Facebook));
    }
    
    subscript (index: Int) -> Feature{
        get{
            return self.features[index];
        }
    }
    
    func featureValues (_ feature: ApplicationFeatureType) -> FeatureTupple{
        switch feature {
        case .PalmReader:
            return (ApplicationFeatureType.PalmReader.rawValue,
                    IconFontCodes.collection_video,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        case .MoreApps:
            return (ApplicationFeatureType.MoreApps.rawValue,
                    IconFontCodes.shopping_cart_plus,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        case .Feedback:
            return (ApplicationFeatureType.Feedback.rawValue,
                    IconFontCodes.mail_send,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        case .InApps:
            return (ApplicationFeatureType.InApps.rawValue,
                    IconFontCodes.shopping_cart_plus,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        case .Facebook:
            return (ApplicationFeatureType.Facebook.rawValue,
                    IconFontCodes.facebook,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        case .RestoreInApps:
            return (ApplicationFeatureType.RestoreInApps.rawValue,
                    IconFontCodes.time_restore_setting,
                    nil,
                    ColorUtils.instance().featureColor(type: .Background),
                    ColorUtils.instance().featureColor(type: .ShadowColor),
                    ColorUtils.instance().featureShadowRadius());
        default:
            return defFeatureTupple;
        }
    }
}
