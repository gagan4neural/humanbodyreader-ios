//
//  HumanBodyReaderViewController.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 24/05/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import UIKit
import NeuralLabs_iOS

class HumanBodyReaderViewController: BaseViewController {

    private var applicationFeatureType: ApplicationFeatureType = .UnKnown;
    private var btnBack: IconButton? = nil;
    private var cameraView: NNCameraView? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView?.isHidden = true;
        
        self.loadUI();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        self.loadCameraView();
        self.cameraView?.startCamera();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        self.unloadCameraView();
    }
    
    func updateApplicationFeature(type: ApplicationFeatureType) {
        self.applicationFeatureType = type;
    }
    
    func loadUI()
    {
        self.btnBack = IconButton(type: .custom);
        self.btnBack?.setTitle(IconFontCodes.chevronLeft, for: .normal);
        self.btnBack?.setTitleColor(.white, for: .normal);
        self.btnBack?.titleLabel?.font = FontManager.shared.iconFont(size: nlUtils.shared.isIpad() ? 40 : 30);
        self.btnBack?.frame = CGRect(x: 10, y: 10, width: 40, height: 40);
        self.btnBack?.addTarget(self, action: #selector(onBtnTap), for: .touchUpInside)
        
        self.addSubView(view: self.btnBack);
        var dict:[String:UIView] = [:]
        dict["btnBack"] = self.btnBack;
        
        let hVFL_BtnBack = "H:|-10-[btnBack(40)]";
        let vVFL_BtnBack = "V:|-10-[btnBack(40)]";
        
        let hBtnBack = NSLayoutConstraint.constraints(withVisualFormat: hVFL_BtnBack, options: [], metrics: nil, views: dict);
        let vBtnBack = NSLayoutConstraint.constraints(withVisualFormat: vVFL_BtnBack, options: [], metrics: nil, views: dict);
        
        self.containerView.addConstraints(hBtnBack);
        self.containerView.addConstraints(vBtnBack);
    }
    
    
    
    //MARK: Callbacks
    
    @objc func onBtnTap(_ sender: IconButton){
        if (sender.isEqual(self.btnBack)){
            self.safePop(animated: true)
        }
    }
    
    
    //MARK: Invocations
    
    func loadRecogonizerView(objects: [NNCapturedFrameObject]) {
        let vc = HumanBodyRecogonizerViewController();
        vc.update(applicationFeatureType: .PalmReader, objects: objects);
        vc.safePush(animated: true);
    }
    
    
    //MARK: Camera View
    func loadCameraView() {
        if (self.cameraView != nil){
            Logger.shared.log("Camera View Already loaded")
            return;
        }
        
        self.cameraView = NNCameraView(frame: self.containerView.bounds);
        self.containerView.insertSubview(self.cameraView!, belowSubview: self.btnBack!);
        
        self.cameraView?.autoresizingMask = [.flexibleWidth, .flexibleHeight];
        self.cameraView?.recogonizeHumanBodyType(.palm, startImmediate: true, callback: {[weak self] (objects, type) in
            if let obj = objects {
                self?.loadRecogonizerView(objects: obj);
            }
        });
    }
    
    func unloadCameraView() {
        self.cameraView?.pauseCamera();
    }
}
