//
//  HumanBodyRecogonizerViewController.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 11/07/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import UIKit
import NeuralLabs_iOS

class HumanBodyRecogonizerViewController: BaseViewController {

    private var applicationFeatureType: ApplicationFeatureType = .UnKnown;
    private var btnBack: IconButton? = nil;
    private var imageView: NeuralBaseImageView? = nil;
    private var objects: [NNCapturedFrameObject] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView?.isHidden = true;
        
        self.loadUI();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.showImages();
    }

    func loadUI()
    {
        self.btnBack = IconButton(type: .custom);
        self.btnBack?.setTitle(IconFontCodes.chevronLeft, for: .normal);
        self.btnBack?.setTitleColor(.white, for: .normal);
        self.btnBack?.titleLabel?.font = FontManager.shared.iconFont(size: nlUtils.shared.isIpad() ? 40 : 30);
        self.btnBack?.frame = CGRect(x: 10, y: 10, width: 40, height: 40);
        self.btnBack?.addTarget(self, action: #selector(onBtnTap), for: .touchUpInside)
        self.addSubView(view: self.btnBack);
        
        
        let rect = CGRect(x: 10, y: 10, width: self.containerView.bounds.width - 20, height: self.containerView.bounds.height - 20);
        self.imageView = NeuralBaseImageView(frame: rect);
        self.imageView?.backgroundColor = .clear;
        self.imageView?.contentMode = .scaleAspectFit;
        self.imageView?.translatesAutoresizingMaskIntoConstraints = false;
        self.addSubView(view: self.imageView);
        self.containerView.bringSubviewToFront(self.btnBack!);
        
        var dict:[String:UIView] = [:]
        dict["btnBack"] = self.btnBack;
        dict["imageView"] = self.imageView;

        let hVFL_BtnBack = "H:|-10-[btnBack(40)]";
        let vVFL_BtnBack = "V:|-10-[btnBack(40)]";
        
        let hVFL_imageView = "H:|-10-[imageView(\(rect.width)@750)]-10-|";
        let vVFL_imageView = "V:|-10-[imageView(\(rect.height)@750)]-10-|";

        let hBtnBack = NSLayoutConstraint.constraints(withVisualFormat: hVFL_BtnBack, options: [], metrics: nil, views: dict);
        let vBtnBack = NSLayoutConstraint.constraints(withVisualFormat: vVFL_BtnBack, options: [], metrics: nil, views: dict);

        let hImageView = NSLayoutConstraint.constraints(withVisualFormat: hVFL_imageView, options: [], metrics: nil, views: dict);
        let vImageView = NSLayoutConstraint.constraints(withVisualFormat: vVFL_imageView, options: [], metrics: nil, views: dict);

        self.containerView.addConstraints(hBtnBack);
        self.containerView.addConstraints(vBtnBack);
        self.containerView.addConstraints(hImageView);
        self.containerView.addConstraints(vImageView);
    }

    func update(applicationFeatureType: ApplicationFeatureType, objects: [NNCapturedFrameObject]) {
        self.applicationFeatureType = applicationFeatureType;
        self.objects = objects;
    }

    func showImages() {
        self.imageView?.image = self.objects[0].capturedImage;
    }
    
    //MARK: Callbacks
    
    @objc func onBtnTap(_ sender: IconButton){
        if (sender.isEqual(self.btnBack)){
            self.safePop(animated: true)
        }
    }

}
