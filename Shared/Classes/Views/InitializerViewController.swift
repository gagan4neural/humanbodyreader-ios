//
//  InitializerViewController.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 19/05/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import Foundation
import NeuralLabs_iOS


fileprivate let segue_InitializerToMain = "InitializerToMain"


class InitializerViewController: BaseViewController {
    
    private var mainViewLoaded: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NeuralLabManager.shared.initialize();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        if NeuralLabManager.shared.isFirebaseInitialized == false {
            nlUtils.shared.showHud("Initializing application", on: self.view);
        }else{
            self.loadMainView();
        }
    }
    
    func loadMainView () {
        if (self.mainViewLoaded == true){
            Logger.shared.log("Main View Controller already loaded");
            return;
        }
        self.mainViewLoaded = true;
        self.performSegue(withIdentifier: segue_InitializerToMain, sender: self);
    }

    override func layoutAllowsBannerAds() -> Bool {
        return false;
    }

    override func firebaseApplicationInitialized(_ success:Bool){
        super.firebaseApplicationInitialized(success);
        nlUtils.shared.hideHud(from: self.view);
        self.loadMainView();
    }
    override func firebaseVendorInitialized(_ success:Bool){
        super.firebaseVendorInitialized(success);
    }

    //MARK: segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == segue_InitializerToMain){
            return true;
        }
        return false;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segue_InitializerToMain{
            //TODO: Do something interesting here...
        }
    }
    

}
