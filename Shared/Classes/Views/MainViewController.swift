//
//  MainViewController.swift
//  HumanBodyReader-iOS
//
//  Created by Gagan on 19/05/19.
//  Copyright © 2019 Neural Labs. All rights reserved.
//

import Foundation
import UIKit
import NeuralLabs_iOS


fileprivate let MainViewCollectionMinimumHeight:CGFloat = 60;

fileprivate let segue_MainToPalmReader = "MainToPalmReader"


class MainViewController: BaseViewController {
    
    let dataSource = ApplicationFeatureModel();
    var currentFeature: ApplicationFeatureType = .UnKnown;

    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.hideBackButton();
        self.loadCollectionView(withNibName: ApplicationFeatureGridCell.nibName, reuseIdentifier: ApplicationFeatureGridCell.reuseIdentifier);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.adjustViewInsets();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        self.refreshView();
    }
    
    
    //MARK: Layout
    
    override func leftInsetMargin() -> CGFloat{
        return 20
    }
    
    override func rightInsetMargin() -> CGFloat{
        return 20
    }
    
    override func topInsetMargin() -> CGFloat {
        let ret = super.topInsetMargin() + 20;
        return ret;
    }

    
    
    //MARK: Collection View
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.features.count;
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell: ApplicationFeatureGridCell = ApplicationFeatureGridCell.dequeue(collectionView, indexPath: indexPath, reuseIdentifier: ApplicationFeatureGridCell.reuseIdentifier, nibName: ApplicationFeatureGridCell.nibName, cellBundle: Bundle.main)
        cell.baseInit();
        
        if (self.dataSource[indexPath.row].image != nil){
            cell.image.image = UIImage(named: self.dataSource[indexPath.row].image!);
        }else{
            cell.icon.text = self.dataSource[indexPath.row].icon;
        }
        cell.title.textColor = self.dataSource[indexPath.row].titleColor;
        cell.icon.textColor = self.dataSource[indexPath.row].titleColor;
        cell.bgView.backgroundColor = self.dataSource[indexPath.row].bgColor;
        cell.setTitle(self.dataSource[indexPath.row].title);
        
        collectionViewReusage[indexPath] = cell;
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var rect: CGSize = CGSize(width: MainViewCollectionMinimumHeight, height: MainViewCollectionMinimumHeight)
        if nlUtils.shared.isIpad(){
            rect.width = self.containerView.frame.width / 2
        }else{
            rect.width = self.containerView.frame.width
        }
        return rect;
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? ApplicationFeatureGridCell{
            cell.bgView.layer.borderWidth = nlUtils.shared.selectedCellBorderWidth;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? ApplicationFeatureGridCell{
            cell.bgView.layer.borderWidth = 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentFeature = self.dataSource[indexPath.row].feature;
        
        switch (self.currentFeature) {
        case .PalmReader:
            self.loadPalmReader();
            break;
        case .MoreApps:
            self.loadMoreApps();
            break;
        case .InApps:
            self.loadInApps();
            break;
        case .Feedback:
            self.loadFeedback();
            break;
        case .About:
            self.loadAbout();
            break;
        case .Facebook:
            self.loadFacebook();
            break;
        case .RestoreInApps:
            self.restoreInApps();
            break;
        default:
            break;
        }
    }

    
    
    //MARK: Palm Reader
    func loadPalmReader() {
        let vc = HumanBodyReaderViewController();
        vc.updateApplicationFeature(type: .PalmReader);
        vc.safePush(animated: true);
    }
    
    
    
    //MARK: Others
    func loadMoreApps() {
        NeuralLabManager.launchVendor();
    }
    
    func loadInApps() {
        let vc = InAppViewController();
        self.presentViewController(vc);
    }
    
    func loadFeedback() {
        let fvc: FeedbackMailViewController? = FeedbackMailViewController.mailComposer(withTarget: nil);
        
        if (fvc != nil){
            fvc!.setSubject(nlUtils.shared.feedbackSubject, body: nlUtils.shared.feedbackMsg, to: nlUtils.shared.feedbackReciptant);
            if (nlUtils.shared.isIpad()){
                fvc?.modalPresentationStyle = .formSheet;
            }
            nlUtils.shared.applicationNavigationController()?.present(fvc!, animated: true, completion: nil);
        }else{
            self.showToast("Unable to send feedback");
        }
    }
    
    func loadAbout() {
        // Not implemented
    }
    
    func loadFacebook() {
        NeuralLabManager.launchFacebook();
    }
    
    func restoreInApps() {
        self.restoreInAppsProducts();
    }

    
    
    //MARK: segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == segue_MainToPalmReader){
            return true;
        }
        return false;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segue_MainToPalmReader{
        }
    }

}
